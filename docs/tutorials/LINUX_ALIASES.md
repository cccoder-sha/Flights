# Linux Aliases

### git 
Щоб зберегти зміни проекта в GitLab необхідно виконати

`cd path/to/project` 
`git add .`  
`git commit -m "Message for this commit"`  
`git push`  

### alias
Можна зекономити час і набирати значно коротші команди, наприклад

`go2proj`  
`ga .`  
`gc Mesage for this commit`  
`gp`

### .bashrc
для цього треба відредагувати файл `~/.bashrc` на MacOS `~/.profile`.

 Перший варіант:  
 `echo 'alias go2chess="cd /path/to/project/chess"' >> ~/.bashrc` 
 ми додали рядок. Перевіримо
 `tail ~/.bashrc`
 
 Другий варіант:  
 `vi ~/.bashrc`  
 натискаємо `i`  
 пишемо `alias go2chess="cd /path/to/project/chess"`  
 `Esc`  `Shift :`  `wq`  `Enter`
 
### Тестуємо
 Відкриваємо **новий термінал**  
 пишемо `go2chess` і нас має перекинути на директорію проекта.
 Або `go2` натискаємо `tab + tab` і у нас доповнюється команда або показуються всі варіанти,
 що починаються з go2.
 
 Щоб зміни спрацювати в тому ж терміналі, треба виконати  
 `source ~/.bashrc`
 
### Більше варіантів

Додаво в той же файт інші синоніми

```bash
alias go2chess="cd /path/to/chess/project"
alias ga="git add $@"
alias gc="git commit -m $@"
alias gc="git push"
alias gs="git status"
alias gsave="git add . && git commit -m 'update' && git push"
alias ll="ls -la -G" # show all files with colors
alias gd="git diff"
```
 
