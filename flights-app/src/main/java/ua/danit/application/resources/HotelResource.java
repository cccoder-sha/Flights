package ua.danit.application.resources;

import org.codehaus.jackson.map.ObjectMapper;
import ua.danit.application.dao.HotelDao;
import ua.danit.application.model.Hotel;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.io.IOException;

public class HotelResource {

private HotelDao hotelDao;

    @Path("/hotels/top")
    @GET
    public String getTopHotels() throws IOException {
        if (hotelDao == null) {
            hotelDao = new HotelDao();
        }

        Iterable<Hotel> hotels = hotelDao.getTopHotel(5);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(hotels);
    }

    @Path("/hotels/search")
    @GET
    public String getSearchHotels(@QueryParam("search") String search) throws IOException {
        if (hotelDao == null) {
            hotelDao = new HotelDao();
        }

        Iterable<Hotel> hotels = hotelDao.searchHotels(search);
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(hotels);
    }
}
